package com.lynden.gmapsfx;

import com.lynden.gmapsfx.javascript.event.UIEventHandler;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.Animation;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.InfoWindow;
import com.lynden.gmapsfx.javascript.object.InfoWindowOptions;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MVCArray;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Example Application for creating and loading a GoogleMap into a JavaFX
 * application
 *
 * @author Rob Terpilowski
 */
public class MainApp extends Application implements MapComponentInitializedListener {

    protected GoogleMapView mapComponent;
    protected GoogleMap map;

    private Button btnZoomIn;
    private Button btnZoomOut;
    private Label lblZoom;
    private Label lblCenter;
    private Label lblClick;
    private ComboBox<MapTypeIdEnum> mapTypeCombo;
	
	private MarkerOptions markerOptions2;
        private MarkerOptions markerOptions3;
	private Marker myMarker2;
        private Marker myMarker3;
	private Button btnHideMarker;
	private Button btnDeleteMarker;
	
    @Override
    public void start(final Stage stage) throws Exception {
        mapComponent = new GoogleMapView();
        mapComponent.addMapInializedListener(this);
        
        BorderPane bp = new BorderPane();
        ToolBar tb = new ToolBar();

        btnZoomIn = new Button("Zoom In");
        btnZoomIn.setOnAction(e -> {
            map.zoomProperty().set(map.getZoom() + 1);
        });
        btnZoomIn.setDisable(true);

        btnZoomOut = new Button("Zoom Out");
        btnZoomOut.setOnAction(e -> {
            map.zoomProperty().set(map.getZoom() - 1);
        });
        btnZoomOut.setDisable(true);

        lblZoom = new Label();
        lblCenter = new Label();
        lblClick = new Label();
        
        mapTypeCombo = new ComboBox<>();
        mapTypeCombo.setOnAction( e -> {
           map.setMapType(mapTypeCombo.getSelectionModel().getSelectedItem() );
        });
        mapTypeCombo.setDisable(true);
        
        Button btnType = new Button("Map type");
        btnType.setOnAction(e -> {
            map.setMapType(MapTypeIdEnum.ROADMAP);
        });
		
		btnHideMarker = new Button("Hide Markers");
		btnHideMarker.setOnAction(e -> {hideMarker();});
		
		btnDeleteMarker = new Button("Delete Marker");
		btnDeleteMarker.setOnAction(e -> {deleteMarker();});
		
        tb.getItems().addAll(btnZoomIn, btnZoomOut, mapTypeCombo,
                new Label("Zoom: "), lblZoom,
                new Label("Center: "), lblCenter,
                new Label("Click: "), lblClick,
				btnHideMarker, btnDeleteMarker);

        bp.setTop(tb);
        bp.setCenter(mapComponent);

        Scene scene = new Scene(bp);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void mapInitialized() {
        //Once the map has been loaded by the Webview, initialize the map details.        
        LatLong center = new LatLong(25.650314, -100.353437);
        mapComponent.addMapReadyListener(() -> {
            // This call will fail unless the map is completely ready.
            checkCenter(center);
        });
        
        MapOptions options = new MapOptions();
        options.center(center)
                .mapMarker(true)
                .zoom(14)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(true)
                .mapType(MapTypeIdEnum.ROADMAP);

        map = mapComponent.createMap(options);
        
        map.setHeading(123.2);
        System.out.println("Heading is: " + map.getHeading() );
            
        MarkerOptions markerOptions = new MarkerOptions();
        LatLong markerLatLong = new LatLong(25.650314, -100.353437);
        markerOptions.position(markerLatLong)
                .title("Pickup Location")
                .animation(Animation.DROP)
                .visible(true);

        final Marker myMarker = new Marker(markerOptions);

        String resultFreightCostByTruck = getTrucks("http://10.15.173.141:81/MySQLAPI/api/TruckFinder/FreightCostByTruck/0", "GET");
        JSONArray freightCostsByTruck = getJsonArrayFromObject(resultFreightCostByTruck, "Table");
        HashMap<Integer, Double> freightCostByTruck = new HashMap<>();
        
        for (int i = 0; i < freightCostsByTruck.length(); i++) {
            JSONObject value = freightCostsByTruck.getJSONObject(i);
            freightCostByTruck.put(value.getInt("idTrucks"), value.getDouble("priceXkm"));
        }
        
        String result = getTrucks("http://10.15.173.141:81/MySQLAPI/api/TruckFinder/AvailableTrucks/0", "GET");
        JSONArray trucks = getJsonArrayFromObject(result, "Table");

        LatLong mLatLong = null;

        double maxDistance = 0;
        JSONArray routesMax = null;
        Polyline[] actualPoly = {null};

        InfoWindowOptions infoOptions = new InfoWindowOptions();
        InfoWindow window = new InfoWindow(infoOptions);
        
        for (int i = 0; i < trucks.length(); i++) {
            JSONObject truck = trucks.getJSONObject(i);
            MarkerOptions mOptions = new MarkerOptions();
            mLatLong = new LatLong(truck.getDouble("Pos_lat"), truck.getDouble("Pos_long"));
            mOptions.position(mLatLong)
                .title(String.valueOf(truck.getInt("idTrucks")))
                .icon("ttype" + truck.getInt("idTruck_types") +"_icon.png")                 
                .visible(true);
           
            String url= "https://maps.googleapis.com/maps/api/directions/json?origin=" 
            + markerLatLong.getLatitude() + "," + markerLatLong.getLongitude() +"&destination=" 
            + mLatLong.getLatitude() + "," + mLatLong.getLongitude() + "&sensor=false&key=AIzaSyD3b_ij9ivqAwc-oyywx5hyigV1Vr2QadA";
        
            JSONArray routes = getJsonArrayFromObject(getTrucks(url, "GET"), "routes");
            double distanceForSegment = routes.getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getInt("value");

            if (distanceForSegment>maxDistance) {
                routesMax = routes;
                
                maxDistance = distanceForSegment;
            }
            
            DecimalFormat df = new DecimalFormat("#.##");
            
            Marker mark = new Marker(mOptions);
            map.addMarker(mark);
            map.addUIEventHandler(mark, UIEventType.click, new UIEventHandler() {
                @Override
                public void handle(JSObject obj) {
                    infoOptions.content("<img src=\"ttype" + truck.getInt("idTruck_types") + "_icon.png\" width=\"100\" height=\"30\"/>"
                            + "<h3>Truck Number: " + String.valueOf(truck.getInt("idTrucks")) + 
                            " </h3><h3>Truck Type: " + truck.getString("truck_type_desc") + "</h3>" + 
                            " </h3><h3>Distance: " + (distanceForSegment)/1000 +" Km</h3>" + 
                            " </h3><h3>Price x KM: " + freightCostByTruck.get(truck.getInt("idTrucks")) +" MXN</h3>" + 
                            " </h3><h3>Total Price: " + df.format(freightCostByTruck.get(truck.getInt("idTrucks"))*((distanceForSegment)/1000)) +" MXN</h3>")
                    .position(center);

                    window.setOptions(infoOptions);
                    window.open(map, mark);
                    
                    LatLong ll = new LatLong((JSObject) obj.getMember("latLng"));
                    
                    String urlAux= "https://maps.googleapis.com/maps/api/directions/json?origin="
                            + markerLatLong.getLatitude() + "," + markerLatLong.getLongitude() +"&destination="
                            + ll.getLatitude() + "," + ll.getLongitude() + "&sensor=false&key=AIzaSyD3b_ij9ivqAwc-oyywx5hyigV1Vr2QadA";
                    
                    JSONArray routesAux = getJsonArrayFromObject(getTrucks(urlAux, "GET"), "routes");
                    
                    JSONArray stepsAux = routesAux.getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");
                    
                    List<LatLong> lines = new ArrayList<LatLong>();
                    
                    for(int j=0; j < stepsAux.length(); j++) {
                        String polyline = stepsAux.getJSONObject(j).getJSONObject("polyline").getString("points");
                        
                        for(LatLong p : decodePolyline(polyline)) {
                            lines.add(p);
                        }
                    }
                    
                    MVCArray mvc = new MVCArray(lines.toArray());
                    
                    PolylineOptions polyOpts = new PolylineOptions()
                            .geodesic(true)
                            .path(mvc)
                            .strokeColor("red")
                            .strokeWeight(2);
                    
                    Polyline poly = new Polyline(polyOpts);
                    map.removeMapShape(actualPoly[0]);
                    map.addMapShape(poly);
                    actualPoly[0] = poly;
                    System.out.println("You clicked the line at LatLong: lat: " + ll.getLatitude() + " lng: " + ll.getLongitude());
                }
            });
        }
        
        JSONArray steps = routesMax.getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");

        List<LatLong> lines = new ArrayList<LatLong>();
        
        for(int i=0; i < steps.length(); i++) {
            String polyline = steps.getJSONObject(i).getJSONObject("polyline").getString("points");

            for(LatLong p : decodePolyline(polyline)) {
                lines.add(p);
            }
        }

        MVCArray mvc = new MVCArray(lines.toArray());
        
        PolylineOptions polyOpts = new PolylineOptions()
                .geodesic(true)
                .path(mvc)
                .strokeColor("red")
                .strokeWeight(2);

        Polyline poly = new Polyline(polyOpts);
        
        actualPoly[0] = poly;
        
        map.addMapShape(poly);
        map.addUIEventHandler(poly, UIEventType.click, (JSObject obj) -> {
            LatLong ll = new LatLong((JSObject) obj.getMember("latLng"));
//            System.out.println("You clicked the line at LatLong: lat: " + ll.getLatitude() + " lng: " + ll.getLongitude());
        });
               
        map.addMarker(myMarker);

        infoOptions.content("<img src=\"Neoris_Logo_small.jpg\" width=\"100\" height=\"30\"/><h3>Construrama Santa Barbara</h3><h3>Truck type A</h3><h3>Heavy Load</h3>")
                .position(center);

        window.setOptions(infoOptions);
        window.open(map, myMarker);
        
       
        //map.fitBounds(new LatLongBounds(center, new LatLong(25.656227, -100.295223)));
        //System.out.println("Bounds : " + map.getBounds());
        map.zoomProperty().set(13); 

        lblCenter.setText(map.getCenter().toString());
        map.centerProperty().addListener((ObservableValue<? extends LatLong> obs, LatLong o, LatLong n) -> {
            lblCenter.setText(n.toString());
        });
         

        lblZoom.setText(Integer.toString(map.getZoom()));
        map.zoomProperty().addListener((ObservableValue<? extends Number> obs, Number o, Number n) -> {
            lblZoom.setText(n.toString());
        });
        
        map.addUIEventHandler(UIEventType.click, (JSObject obj) -> {
            LatLong ll = new LatLong((JSObject) obj.getMember("latLng"));
                        
            System.out.println("LatLong: lat: " + ll.getLatitude() + " lng: " + ll.getLongitude());
            lblClick.setText(ll.toString());
        });

        btnZoomIn.setDisable(false);
        btnZoomOut.setDisable(false);
        mapTypeCombo.setDisable(false);
        
        mapTypeCombo.getItems().addAll( MapTypeIdEnum.ALL );
    }
	
	
	private void hideMarker() {
//		System.out.println("deleteMarker");
		
		boolean visible = myMarker2.getVisible();
		
		//System.out.println("Marker was visible? " + visible);
		
		myMarker2.setVisible(!visible);

//				markerOptions2.visible(Boolean.FALSE);
//				myMarker2.setOptions(markerOptions2);
//		System.out.println("deleteMarker - made invisible?");
	}
	
	private void deleteMarker() {
		//System.out.println("Marker was removed?");
		map.removeMarker(myMarker2);
	}
	
    private void checkCenter(LatLong center) {
//        System.out.println("Testing fromLatLngToPoint using: " + center);
//        Point2D p = map.fromLatLngToPoint(center);
//        System.out.println("Testing fromLatLngToPoint result: " + p);
//        System.out.println("Testing fromLatLngToPoint expected: " + mapComponent.getWidth()/2 + ", " + mapComponent.getHeight()/2);
    }
   
        protected String getTrucks(String uri, String type) {
            String responseText = "";

            try {
                InputStreamReader input = null;
                BufferedReader inputBuffer = null;
                String responseString = "";
                
                HttpURLConnection httpclient = (HttpURLConnection) new URL(uri).openConnection();
                httpclient.setUseCaches(false);
                httpclient.setConnectTimeout(1000);
                httpclient.setRequestMethod(type);
                httpclient.setRequestProperty("Accept", "*/*");
                httpclient.setRequestProperty("Content-Type", "application/atom+xml");

                if (type == "POST")
                    httpclient.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                int responseCode = httpclient.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {

                    input = new InputStreamReader(httpclient.getInputStream(), "UTF-8");
                    inputBuffer = new BufferedReader(input);

                    while ((responseString = inputBuffer.readLine()) != null) {
                        responseText += responseString;
                    }

                    input.close();
                    inputBuffer.close();

                    //Closes the connection.
                    httpclient.disconnect();
                } else {
                    input = new InputStreamReader(httpclient.getErrorStream(), "UTF-8");
                    inputBuffer = new BufferedReader(input);

                    while ((responseString = inputBuffer.readLine()) != null) {
                        responseText += responseString;
                    }

                    input.close();
                    inputBuffer.close();

                    //Closes the connection.
                    httpclient.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseText;
        }
                
    public static JSONArray getJsonArrayFromObject(String json, String object) {
        JSONArray jArrayResult = null;

        if (json!=null && !json.trim().equals("")) {
            try {
                JSONObject jObject = new JSONObject(json);

                jArrayResult = jObject.getJSONArray(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return jArrayResult;
    }
    
    private List<LatLong> decodePolyline(String encoded) {

        List<LatLong> poly = new ArrayList<LatLong>();

        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLong p = new LatLong((double) lat / 1E5, (double) lng / 1E5);
            poly.add(p);
        }

        return poly;
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.setProperty("java.net.useSystemProxies", "true");
        launch(args);
    }
}
